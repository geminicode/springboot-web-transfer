import json
import sys,os
import uuid

CASPRT_GROUP = "JSAT-NEOS"

class CasportGroup:

  reviewer_roles = [ "reference_data_view",
        "stats_staff_roles",
        "stats_events",
        "event_view",
        "version_view"]

  coordinator_roles = ["event_create",
        "identifier_create",
        "reply_create",
        "stats_staff_roles",
        "location_create",
        "operational_impact_update",
        "reply_update",
        "event_view",
        "reference_data_view",
        "operational_impact_create",
        "location_update",
        "stats_events",
        "named_filter_create",
        "event_update",
        "identifier_update",
        "version_view"

  coordinator_roles = ["custom_field_create",
        "custom_field_update",
        "identifier_delete",
        "reply_create",
        "operation_delete",
        "location_create",
        "staff_role_create",
        "staff_role_update",
        "operational_impact_update",
        "reply_update",
        "staff_role_delete",
        "location_delete",
        "location_update",
        "identifier_update",
        "reply_delete",
        "event_create",
        "identifier_create",
        "stats_staff_roles",
        "status_create",
        "event_delete",
        "operation_create",
        "event_view",
        "status_update",
        "reference_data_view",
        "operational_impact_create",
        "stats_events",
        "named_filter_create",
        "event_update",
        "operation_update",
        "tracker_create",
        "tracker_update",
        "tracker_delete",
        "version_view" ]

  admin_roles = ["custom_field_create",
        "custom_field_update",
        "identifier_delete",
        "reply_create",
        "operation_delete",
        "location_create",
        "status_delete",
        "staff_role_create",
        "staff_role_update",
        "operational_impact_update",
        "reply_update",
        "classification_update",
        "staff_role_delete",
        "location_delete",
        "location_update",
        "classification_create",
        "identifier_update",
        "operational_impact_delete",
        "reply_delete",
        "event_create",
        "identifier_create",
        "stats_staff_roles",
        "status_create",
        "event_type_create",
        "event_type_update",
        "classification_delete",
        "event_delete",
        "operation_create",
        "event_view",
        "status_update",
        "event_type_delete",
        "reference_data_view",
        "event_type_update",
        "operational_impact_create",
        "stats_events",
        "event_update",
        "operation_update",
        "named_filter_create",
        "tracker_create",
        "tracker_update",
        "tracker_delete",
        "version_view"
  ]

  def __init__(self, projectName):
      self.projectName = projectName

    pass

class CasportUser:

  uniqueId = "" 

  def __init__(self, distinguishedName):
    self.dn = distinguishedName
    self.distinguishedName = distinguishedName
    self.displayName = distinguishedName.split(",")[0].split("=")[1]
    names = self.displayName.split(".")
    self.lastName = names[0]
    self.firstName = names[1]
    self.middleName = names[2]
    if len(names) > 3:
      self.employeeId = names[3]
    self.fullName = self.firstName + ' ' + self.lastName
    self.email = self.firstName + '.' + self.lastName + '@afs.com'
    self.uniqueId = str(uuid.uuid4())

    self.personalTitle = "CONTRACTOR" 
    self.secureTelephoneNumber = "555-5678" 
    self.telephoneNumber = "703//555-1234" 
    self.clearances = [ "TS", "S", "C", "U" ]
    self.fineAccessControls = [ "TK","SI","GAMMA","HCS","G"],
    self.formalAccesses = ["TK","SI","GAMMA","HCS","G"],
    self.cois = ["OSCAR"],
    self.acgs = [],
    self.accms = ["ACCMS1","ACCMS2"],
    self.briefings = ["OVSC1300","IKE"],
    self.lacs = ["IC_DATA","IC-DATA"],
    self.visas = [ "NZL","USA","AUS","CAN","FVEY","GBR"],
    self.isIcMember = False
    self.formalGroups = [],
    self.citizenshipStatus = "US",
    self.country = "USA",
    self.grantBy = [],
    self.organizations = ["H3C"],
    self.affliliations = [],
    self.title = "...",
    self.dissemControl = ["FOUO","NOFORN","FISA","ORCON","RS","OC","NF"],
    self.dissemTo = [],
    self.subRegion = [],
    self.topic = [],
    self.isAicp = False,
    self.dutyorg = "CYBERCOM",
    self.missionId = "...",
    self.cr1423 = []


  def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)

if __name__ == '__main__':
    user = CasportUser("CN=FULLER.DARRELL.LEE.1128379483,OU=CONTRACTOR,OU=PKI,OU=DoD,O=U.S. Government,C=US")
    print(json.dumps(user.toJson()))
    sys.exit(0) 
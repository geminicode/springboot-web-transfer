package com.geminicode.webtransfer.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import com.geminicode.webtransfer.util.LoggingUtil;

/**
 * Spring Web Security Configuration
 *
 */
@Configuration
public class WebSecurityConfig {
    
    @Value("${security.enable-cors:true}")
    private boolean corsEnabled;
    
    @Value("${security.enable-csrf:true}")
    private boolean csrfEnabled;
    
    @Value("${security.enable-same-origin:true}")
    private boolean sameOriginEnabled;    

    /**
     * Configure URI Path Security
     * @param http security configuration
     * @throws Exception during configuration
     */
    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        LoggingUtil.logInfo("** Configuring HttpSecurity for Swagger URLs.");
        http.authorizeRequests()
                .antMatchers("/swagger-ui.html", "/swagger-ui/**",
                     "/actuator/health/readiness", "/actuator/health/liveness",
                     "/attachement/**", "/upload**", "/**/*.css", "/**/*.js","/index.html",
                     "/api-docs", "/api-docs/**")
                    .permitAll()
                .anyRequest().authenticated();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        if (corsEnabled) {
            http.cors();
        }
        if (sameOriginEnabled) {
            http.headers().frameOptions().sameOrigin();
        }
        if (csrfEnabled) {
            http.csrf().csrfTokenRepository(getCsrfTokenRepository());
        } else {
            http.csrf().disable(); // DLF: SpotBugs does not like disabling of CSRF()
        }
        return http.build();
    }
    
    private CsrfTokenRepository getCsrfTokenRepository() {
        var csrfTokenRepository = CookieCsrfTokenRepository.withHttpOnlyFalse();
        csrfTokenRepository.setCookiePath("/");
        return csrfTokenRepository;
    }
}

package com.geminicode.webtransfer.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import com.geminicode.webtransfer.model.AttachmentFile;
import com.geminicode.webtransfer.service.AttachmentFileService;
import com.geminicode.webtransfer.util.LoggingUtil;
import com.geminicode.webtransfer.util.ResponseMessagesUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
public class AttachmentController {

    @Autowired
    private AttachmentFileService attachmentFileService;
    
    @Value("${webstransfer.attachements.upload-temp-dir}")
    private String uploadTempDir;

    /**
     * Upload an Attachment
     * @return {ModelMap} that contains the Attachement Reference
     */
    @Operation(
            summary = "Upload an Attachment",
            description = "Upload an attachment and return a UUID reference."
    )
    @ApiResponses(value = { // Potential responses sent by the API based on a "success"
            @ApiResponse(responseCode = ResponseMessagesUtils.HTTP_200, description = ResponseMessagesUtils.OK_MSG),
            @ApiResponse(responseCode = ResponseMessagesUtils.HTTP_503, description = ResponseMessagesUtils.INTERNAL_SERVER_ERROR)
    })
    @RequestMapping(value = "/attachement/upload", method = RequestMethod.POST)
    public ResponseEntity<ModelMap> upload(@RequestParam("file") MultipartFile file, ModelMap modelMap) {
        HttpStatus status = HttpStatus.OK;
        try {
            LoggingUtil.logInfo(String.format("Uploading file [%s/%s]", uploadTempDir, file.getOriginalFilename()));
            AttachmentFile attachment = AttachmentFile.builder()
                .contentType(file.getContentType())
                .size(file.getSize())
                .filename(file.getOriginalFilename())
                .content(file.getBytes())
                .build();
            attachment = attachmentFileService.save(attachment);
            modelMap.addAttribute("uuid", attachment.getId());
        } catch (IllegalArgumentException | IOException e) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            LoggingUtil.logError(e.getMessage());
        }
        return new ResponseEntity<>(modelMap, status);
    }
    
    /**
     * View all Attachments
     * @return {String[]} that contains the Attachements
     */
    @Operation(
            summary = "View all Attachments",
            description = "View all attachments."
    )
    @ApiResponses(value = { // Potential responses sent by the API based on a "success"
            @ApiResponse(responseCode = ResponseMessagesUtils.HTTP_200, description = ResponseMessagesUtils.OK_MSG),
            @ApiResponse(responseCode = ResponseMessagesUtils.HTTP_503, description = ResponseMessagesUtils.SERVICE_UNAVAILABLE)
    })
    @RequestMapping(value = "/attachement/list", method = RequestMethod.GET)
    public ResponseEntity<String[]> list() {
        File f = new FileSystemResource(uploadTempDir).getFile();

        String[] pathnames = f.list();

        for (String pathname : pathnames) {
            LoggingUtil.logInfo(pathname);
        }
        return new ResponseEntity<>(pathnames, HttpStatus.OK);
    }

    /**
     * Download an Attachment
     * @return {ModelMap} Download an Attachment by UUID
     */
    @Operation(
            summary = "Download an Attachments",
            description = "Download an Attachment"
    )
    @ApiResponses(value = { // Potential responses sent by the API based on a "success"
            @ApiResponse(responseCode = ResponseMessagesUtils.HTTP_200, description = ResponseMessagesUtils.OK_MSG),
            @ApiResponse(responseCode = ResponseMessagesUtils.HTTP_503, description = ResponseMessagesUtils.SERVICE_UNAVAILABLE)
    })
    @RequestMapping(value = "/attachement/download/{uuid}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> download(@PathVariable("uuid") String uuid) {
        try {
            AttachmentFile file = attachmentFileService.find(UUID.fromString(uuid));
            return ResponseEntity.ok()
                    .header("Content-Disposition", "attachment; filename=" + file.getFilename())
                    .contentType(MediaType.valueOf(file.getContentType()))
                    .contentLength(file.getSize())
                    .body(file.getContent());
        } catch (FileNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Attachement not found!", e);
        }
    }
    
    /*
    private void copy(MultipartFile multipartFile) throws IOException {
        String fileName = multipartFile.getOriginalFilename();
        String normalizedFileName = FilenameUtils.normalize(fileName);
        LoggingUtil.logInfo(String.format("Copying file [%s] to [%s]", normalizedFileName, uploadTempDir));
        multipartFile.transferTo(Path.of(uploadTempDir, normalizedFileName));
    }
    */
}

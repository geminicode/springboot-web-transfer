package com.geminicode.webtransfer;

import java.io.IOException;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.util.unit.DataSize;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import com.geminicode.webtransfer.util.LoggingUtil;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

@SpringBootApplication
public class Application {

    private static final String AUTHORIZATION_NAME = "bearerAuth";
    private static final String TOKEN_TYPE = "JWT";

    @Value("${build.version}")
    private String buildVersion;

    @Value("${spring.servlet.multipart.max-file-size}")
    private String maxFileSize;
    @Value("${spring.servlet.multipart.max-request-size}")
    private String maxRequestSize;
    @Value("${webstransfer.attachements.upload-temp-dir}")
    private String uploadTempDir;

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(Application.class, args);
    }

    /**
     * OpenAPI interface as an alternative to Swagger2
     * @return {OpenAPI}
     */
    @Bean
    public OpenAPI openApiConfig() {
        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList(AUTHORIZATION_NAME))
                .components(
                    new Components()
                        .addSecuritySchemes(AUTHORIZATION_NAME,
                            new SecurityScheme()
                                .name(AUTHORIZATION_NAME)
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("bearer")
                                .bearerFormat(TOKEN_TYPE)
                        )
                )
                .info(new Info().title("Joint Situational Awareness Tool")
                .description("<b>IMPORTANT:</b> Concerning Dates, the server receives dates as an Unix Epoch, which is the number milliseconds since"
                       + "January 1st, 1970 UTC.  The server also runs in UTC, and the server is unaware of the client's"
                       + "timezone.  Date manipulations should occur on the client, which then get converted to UTC during"
                       + "transmission.  If Date's are manipulated on the server, day boundary issues may occur."
                       + "<br><br>"
                       + "When executing api calls, ensure datetime values are already adjusted and valid Epochs")
                .version(buildVersion)
                .license(new License().name("Apache 2.0").url("http://springdoc.org")))
                .externalDocs(new ExternalDocumentation()
                .description("SpringShop Wiki Documentation")
                .url("https://springshop.wiki.github.org/docs"));
    }
    
    /**
     * Apache Commont Uploads MultipartResolver Configuration
     * @return
     */
    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(DataSize.parse(maxRequestSize).toBytes());
        multipartResolver.setMaxUploadSizePerFile(DataSize.parse(maxFileSize).toBytes());
        try {
            multipartResolver.setUploadTempDir(new FileSystemResource(uploadTempDir));
        } catch (IOException e) {
            LoggingUtil.logError(e.getMessage());
        }
        return multipartResolver;
    }    
}

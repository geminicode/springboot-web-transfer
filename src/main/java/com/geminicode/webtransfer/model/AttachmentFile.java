package com.geminicode.webtransfer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Column;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * A file uploaded and then stored in the database
 */
@Data
@Entity
@Table(name = "attachment")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AttachmentFile {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator",
        parameters = {
            @Parameter(
                name = "uuid_gen_strategy_class",
                value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
            )
        }
    )
    @Type(type = "uuid-char")
    @Column(name = "id", updatable = false, nullable = false)
    protected UUID id;

    @Column(name = "filename", length = 255, nullable = false)
    private String filename;

    @Column(name = "size", nullable = true)
    private Long size;

    @Column(name = "content_type", length = 50, nullable = true)
    private String contentType;

    @Column(name = "created_by", length = 320, nullable = true)
    private String createdBy;

    @CreatedDate
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    @Column(name = "created_on", nullable = false, columnDefinition = "timestamp with time zone")
    private OffsetDateTime createdOn;
    
    @Lob
    private byte[] content;
}

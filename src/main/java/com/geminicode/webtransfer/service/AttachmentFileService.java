package com.geminicode.webtransfer.service;

import java.io.FileNotFoundException;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.geminicode.webtransfer.model.AttachmentFile;
import com.geminicode.webtransfer.repository.AttachmentFileRepository;

@Service
public class AttachmentFileService {

    @Autowired
    private AttachmentFileRepository attachmentFileRepository;

    /**
     * Save an attachment
     * @param file attachment
     * @return saved attachment
     */
    public AttachmentFile save(AttachmentFile file) {
        attachmentFileRepository.save(file);
        return file;
    }

    /**
     * Find an existing attachment by key
     * @param id of the attachment
     * @return found attachment
     * @throws FileNotFoundException file did not exist
     */
    public AttachmentFile find(UUID id) throws FileNotFoundException {
        Optional<AttachmentFile> file = attachmentFileRepository.findById(id);
        
        if (file.isEmpty()) {
            throw new FileNotFoundException();
        }

        return file.get();
    }    
}

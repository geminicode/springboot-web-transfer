package com.geminicode.webtransfer.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geminicode.webtransfer.model.AttachmentFile;

@Repository
@Transactional
public interface AttachmentFileRepository extends CrudRepository<AttachmentFile, UUID> {

}

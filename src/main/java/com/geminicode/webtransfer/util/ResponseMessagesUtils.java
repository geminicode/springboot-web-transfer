package com.geminicode.webtransfer.util;

/**
 * Class that contains constants for messages that we send back to the client
 */
public final class ResponseMessagesUtils {

    public static final String HTTP_200 = "200";
    public static final String HTTP_201 = "201";
    public static final String HTTP_202 = "202";
    public static final String HTTP_208 = "208";
    public static final String HTTP_500 = "500";
    public static final String HTTP_503 = "503";
    public static final String HTTP_400 = "400";
    public static final String HTTP_403 = "403";

    public static final String OK_MSG = "OK";
    public static final String CONFLICT = "Operation resulted in a data conflict (i.e. duplicate)";
    public static final String ALREADY_REPORTED = "Already Reported";
    public static final String CREATED_MSG = "Created";
    public static final String ACCEPTED = "Accepted";
    public static final String FORBIDDEN = "User Not Authorized";
    public static final String BAD_REQUEST = "Bad Request";
    public static final String SERVICE_UNAVAILABLE = "Service Unavailable";
    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";

    private ResponseMessagesUtils() { }
}

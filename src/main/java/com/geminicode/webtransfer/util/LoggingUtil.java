package com.geminicode.webtransfer.util;

import lombok.extern.log4j.Log4j2;

@Log4j2
public final class LoggingUtil {

    private LoggingUtil() {
        super();
    }

    /**
     * Check for error logging is enabled and log the message if appropriate
     * @param message {String} message to log out
     */
    public static void logDebug(String message) {
        if (log.isDebugEnabled()) {
            log.debug(message.replace('\n', '_').replace('\r', '_'));
        }
    }

    /**
     * Check for error logging is enabled and log the message if appropriate
     * @param message {String} message to log out
     */
    public static void logInfo(String message) {
        if (log.isInfoEnabled()) {
            log.info(message.replace('\n', '_').replace('\r', '_'));
        }
    }

    /**
     * Check for warn logging is enabled and log the message if appropriate
     * @param message {String} message to log out
     */
    public static void logWarn(String message) {
        if (log.isWarnEnabled()) {
            log.warn(message.replace('\n', '_').replace('\r', '_'));
        }
    }

    /**
     * Check for error logging is enabled and log the message if appropriate
     * @param message {String} message to log out
     */
    public static void logError(String message) {
        if (log.isErrorEnabled()) {
            log.error(message.replace('\n', '_').replace('\r', '_'));
        }
    }
}

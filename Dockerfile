FROM openjdk11

ARG JAR_FILE=target/spring-boot-web-transfer-*.jar

ARG APP_DIR=/app

WORKDIR ${APP_DIR}
COPY ${JAR_FILE} ${APP_DIR}/app.jar

USER 1001
CMD ["java", "-jar", "app.jar"]
